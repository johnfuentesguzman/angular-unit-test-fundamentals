import {compute} from './compute';

describe('get the compute function', () => {
    it('should be return 0 if the parameter is empty', () => {
        const result = compute(-1);
        expect(result).toEqual(0);
    })
});