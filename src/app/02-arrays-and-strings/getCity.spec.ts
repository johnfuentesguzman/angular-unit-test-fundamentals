import {getCities} from './getCity';

describe('get the city', () => {
    it('should be return lenght > 0', () => {
        const result = getCities(3000000);
        expect(result).length > 0;
    })
});