import { FormBuilder } from '@angular/forms';
import { TodoFormComponent } from './todo-form.component'; 
describe('TodoFormComponent', () => {
  var component: TodoFormComponent; 

  beforeEach(() => {
    component = new TodoFormComponent(new FormBuilder() );
  });

  it('should make the name form control required ', () => {
    var controls = component.form.get('name');
    controls.setValue('');
    expect(controls.valid).toBeFalsy()
  });
});