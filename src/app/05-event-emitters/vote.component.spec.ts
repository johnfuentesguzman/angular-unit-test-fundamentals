import { VoteComponent } from './vote.component'; 

describe('VoteComponent', () => {
  var component: VoteComponent; 

  beforeEach(() => {
    component = new VoteComponent();
  });

  it('get input chnage event trough event', () => {
    let vote = null;
    component.voteChanged.subscribe(event => {
      vote = event;
      //expect(vote).not.toBeNull()
      expect(vote).toBe(1);
    });
  });
});