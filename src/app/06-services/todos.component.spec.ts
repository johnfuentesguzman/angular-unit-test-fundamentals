import { TodosComponent } from './todos.component'; 
import { TodoService } from './todo.service'; 
//import 'rxjs/add/observable/from';  // create a observable from/using an array
import { from, of, throwError } from 'rxjs';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let service : TodoService;

  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  it('should the endpoint returns a item with all TODOS taks availables', () => {
    spyOn(service, 'getTodos').and.callFake(() => { // GET functions into the service
      const ObsSource = from([ [ 1, 2 , 4] ]); // creating array from an array
      return ObsSource;
    });
    component.ngOnInit();
    expect(component.todos.length).toBeGreaterThan(1)
  });

  it('should be called add  service function into the component function add() ', () => {
    let adMethodService = spyOn(service, 'add').and.callFake((t) => { // GET functions into the service: this case "t" is the parameter that the service expected
      return of(); // returning a empty observable
    });
    component.add();
    expect(adMethodService).toHaveBeenCalled();
  });

  it('should return a ERROR MSS if  todo variable is empty into ADD method componemt ', () => {
    spyOn(service, 'add').and.returnValue(throwError('oops there was a error')); // the same than "callFake", but cleaner
    component.add();
    expect(component.message).not.toBeNull();
  });
  
  it('should delete the todo task if the user confirms ', () => {
    spyOn(window, 'confirm').and.returnValue(true); // the same than "callFake", but cleaner
    let deleteMethodService = spyOn(service, 'delete').and.returnValue(of(true));

    component.delete(1);
    expect(deleteMethodService).toHaveBeenCalledWith(1);
  });

});