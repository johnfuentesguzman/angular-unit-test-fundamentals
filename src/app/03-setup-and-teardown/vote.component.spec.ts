import { VoteComponent } from './vote.component'; 

describe('VoteComponent', () => {
  let component = new VoteComponent;
  beforeEach(() => { // make this after make any single test
    component = new VoteComponent(); // settig the components values and functions to startpoint before to begin any
  });

  it('should increment totalvotes when upvoted', () => {
    component.upVote();
    expect(component.totalVotes).toBe(1);
  });

  it('should dicrement totalvotes when downvoted', () => {
    let component = new VoteComponent();
    component.downVote();
    expect(component.totalVotes).toBe(-1);
  });
});